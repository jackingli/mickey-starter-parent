package com.mickey.redis.lock;

import com.mickey.redis.lock.annotation.DistributedLock;
import com.mickey.core.exception.NoveSystemException;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.core.LocalVariableTableParameterNameDiscoverer;
import org.springframework.core.ParameterNameDiscoverer;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;

import java.lang.reflect.Method;

/**
 * @author J·K
 * @description: RedissonDistributedLockAspect
 * @date 2021/6/10 2:15 下午
 */
@Slf4j
@Aspect
public class RedissonDistributedLockAspect {

    private static final String POINT_CUT = "@annotation(com.mickey.redis.lock.annotation.DistributedLock)";

    private RedissonClient redisson;
    private ExpressionParser expressionParser;
    private ParameterNameDiscoverer parameterNameDiscoverer;

    public RedissonDistributedLockAspect(RedissonClient redisson) {
        this.redisson = redisson;
        this.expressionParser = new SpelExpressionParser();
        this.parameterNameDiscoverer = new LocalVariableTableParameterNameDiscoverer();
    }

    @Pointcut(POINT_CUT)
    public void lockPointCut() {
    }

    @Around("lockPointCut()")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
        Object[] args = joinPoint.getArgs();
        Method method = ((MethodSignature) joinPoint.getSignature()).getMethod();
        DistributedLock lockAnnotation = method.getAnnotation(DistributedLock.class);
        String spEl = lockAnnotation.value();
        String lockKey = parseLockKey(spEl, method, args);

        RLock lock = redisson.getLock(lockKey);
        boolean isLock = lock.tryLock(lockAnnotation.waitTime(), lockAnnotation.leaseTime(), lockAnnotation.unit());
        if (!isLock) {
            log.error(String.format("get lock failed [%s]", lockKey));
            throw new NoveSystemException(lockAnnotation.errorMsg());
        }

        //得到锁,执行方法，释放锁
        log.info(String.format("get lock success [%s]", lockKey));
        try {
            return joinPoint.proceed();
        } catch (Exception e) {
            log.error("execute locked method occured an exception", e);
            throw new NoveSystemException(e.getMessage());
        } finally {
            if (lock.isLocked()) {
                lock.unlock();
                log.debug(String.format("release lock [%s]", lockKey));
            }
        }
    }

    private String parseLockKey(String spEl, Method method, Object[] args) {
        // 这里Spring最终使用的是LocalVariableTableParameterNameDiscoverer类,它对ASM进行了封装,读取了Class文件的LocalVariableTable
        // 来得到方法参数的真正名称,当然这里需要编译器开启输出调试符号信息的参数的-g,生成的Class文件才会带有LocalVariableTable
        // 一般默认都会开启
        String[] params = parameterNameDiscoverer.getParameterNames(method);
        EvaluationContext context = new StandardEvaluationContext();
        for (int i = 0; i < params.length; i++) {
            context.setVariable(params[i], args[i]);
        }
        return expressionParser.parseExpression(spEl).getValue(context, String.class);
    }
}
