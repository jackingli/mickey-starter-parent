package com.mickey.redis.lock.annotation;

import java.lang.annotation.*;
import java.util.concurrent.TimeUnit;

/**
 * @author J·K
 * @description: DistributedLock
 * @date 2021/6/10 2:19 下午
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface DistributedLock {
    /**
     * key for lock,spring El表达式
     * @return
     */
    String value() default "'mickey'";

    /**
     * 获取等待时间
     * @return
     */
    long waitTime() default 3000L;

    /**
     * 自动释放时间 -1会启用看门狗自动续期
     * @return
     */
    long leaseTime() default 30000L;

    /**
     * 时间单位
     * @return
     */
    TimeUnit unit() default TimeUnit.MILLISECONDS;

    /**
     * 异常信息
     * @return
     */
    String errorMsg() default "系统处理异常";
}
