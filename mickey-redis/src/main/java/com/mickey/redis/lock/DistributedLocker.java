package com.mickey.redis.lock;

import org.redisson.api.RLock;

import java.util.concurrent.TimeUnit;

/**
 * @author J·K
 * @Description: TODO
 * @date 2019-06-27 14:50
 */
public interface DistributedLocker {
    /**
     * 没有超时时间,默认30s
     * @param lockKey
     * @return
     */
    RLock lock(String lockKey);
    /**
     * 自己设置超时时间
     * @param lockKey 锁的key
     * @param timeout  秒  如果是-1，直到自己解锁，否则不会自动解锁
     * @return
     */
    RLock lock(String lockKey, int timeout);

    /**
     * 自己设置超时时间
     * @param lockKey 锁的key
     * @param unit 时间单位
     * @param timeout 释放时间
     * @return
     */
    RLock lock(String lockKey, TimeUnit unit, int timeout);
    /**
     *
     * @param lockKey  锁key
     * @param unit  锁单位
     * @param waitTime   等到最大时间，强制获取锁
     * @param leaseTime  锁失效时间 -1会启用看门狗自动续期
     * @return
     */
    boolean tryLock(String lockKey, TimeUnit unit, int waitTime, int leaseTime);

    /**
     * 解锁
     * @param lockKey
     */
    void unlock(String lockKey);

    /**
     * 解锁
     * @param lock
     */
    void unlock(RLock lock);
}
